import React, {Component, Fragment} from 'react';
import './PasswordChecker.css';
import {connect} from "react-redux";

class PasswordChecker extends Component {

    calculateAnswer = () => {
        let answer = '';
        if(this.props.calcOperands && this.props.calcOperators &&
            (this.props.calcOperators.length%2 !== this.props.calcOperands.length%2)){
            answer = this.props.calcOperands[0];
            const operators = this.props.calcOperators;
            const operands = this.props.calcOperands;

            for(let i = 0; i < operators.length; i++) {
                let operator = operators[i];
                console.log(operator);
                switch(operator){
                    case '+': answer += operands[i + 1]; break;
                    case '-': answer -= operands[i + 1]; break;
                    case '*': answer *= operands[i + 1]; break;
                    case '/': answer /= operands[i + 1]; break;
                    default :
                }
            }
            alert('The answer is ' + answer);
        }
        return answer;
    }

    render() {
        let passwordBtns = [];

        for (let i = 0; i < 10; i++) {
            passwordBtns.push(<button onClick={() => this.props.addNum(i)} key={i}>{i}</button>)
        }

        let isTrue = '';
        if (this.props.isTruePassword === null) {
            isTrue = null;
        } else {
            isTrue = this.props.isTruePassword ? <h3>Access granted!</h3> : <h3>Access is not granted!</h3>
        }


        let inputClassName = '';

        if (this.props.isTruePassword === true) {
            inputClassName = 'Passed';
        } else if (this.props.isTruePassword === null) {
            inputClassName = '';
        } else {
            inputClassName = 'Unpassed';
        }


        let calcBtns = [];
        for (let i = 0; i < 10; i++) {
            calcBtns.push(<button onClick={() => this.props.addOperand(i)} key={i}>{i}</button>)
        }
        calcBtns.push(<button onClick={() => this.props.addOperator('-')} key={'-'}>-</button>);
        calcBtns.push(<button onClick={() => this.props.addOperator('+')} key={'+'}>+</button>);
        calcBtns.push(<button onClick={() => this.props.addOperator('/')} key={'/'}>/</button>);
        calcBtns.push(<button onClick={() => this.props.addOperator('*')} key={'*'}>*</button>);

        // const answer = this.calculateAnswer();



        return (
            <Fragment>

                <div className='PasswordChecker'>
                    <input
                        type="password"
                        name='password'
                        placeholder='Enter password'
                        value={this.props.currentPassword}
                        className={inputClassName}
                    />
                    <div className="allBtns">
                        {passwordBtns}
                        <button onClick={this.props.checkPassword}>E</button>
                        <button onClick={this.props.deleteSymbol}>C</button>
                    </div>
                    {isTrue}
                </div>



                <div className="Calculator">
                    <input
                        type="text"
                        name='input'
                        placeholder='Enter your calculations'
                        value={this.props.calculations}
                    />
                    <div className="allBtns">
                        {calcBtns}
                        <button onClick={this.calculateAnswer}>=</button>
                        <button onClick={this.props.deleteCalcSymbol}>delete</button>

                        {/*<button onClick={this.props.deleteSymbol}>C</button>*/}

                    </div>
                </div>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        currentPassword: state.currentPassword,
        truePassword: state.truePassword,
        isTruePassword: state.isTruePassword,

        calcOperands: state.calcOperands,
        calcOperators: state.calcOperators,
        calculations: state.calculations
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNum: (i) => dispatch({type: 'ADD_NUM', number: i}),
        checkPassword: event => dispatch({type: 'CHECK_PASSWORD'}),
        deleteSymbol: event => dispatch({type: 'DELETE_SYMBOL'}),

        addOperand: i => dispatch({type: 'ADD_OPERAND', operand: i}),
        addOperator: i => dispatch({type: 'ADD_OPERATOR', operator: i}),
        deleteCalcSymbol: () => dispatch({type: 'DELETE_CALC_SYMBOL'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChecker);