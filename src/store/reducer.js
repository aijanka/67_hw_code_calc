const initialState = {
    truePassword: '1234',
    currentPassword: '',
    isTruePassword: null,

    calcOperators: [],
    calcOperands: [],
    calculations: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case 'ADD_NUM':
            return {...state, currentPassword: state.currentPassword += action.number.toString()};
        case 'CHECK_PASSWORD':
            return {...state, isTruePassword: state.currentPassword === state.truePassword};
        case 'DELETE_SYMBOL':
            let length = state.currentPassword.length;
            let password = state.currentPassword.substr(0, length-1);
            return {...state, currentPassword: password};
        case 'ADD_OPERAND':
            let calculations = state.calculations;
            calculations += action.operand;
            const calcOperands = [...state.calcOperands];
            calcOperands.push(action.operand);
            console.log(calcOperands);
            return {...state, calcOperands, calculations};
        case 'ADD_OPERATOR':
            const calcOperators = [...state.calcOperators];
            calcOperators.push(action.operator.toString());
            calculations = state.calculations;
            calculations += action.operator.toString();
            console.log(calcOperators);
            return {...state, calcOperators, calculations};
        case 'DELETE_CALC_SYMBOL':
            length = state.calculations.length;
            calculations = state.calculations.substr(0, length-1);
            const symbol = state.calculations[length - 1];
            console.log(symbol);
            if(Number.isInteger(parseInt(symbol))){
                const calcOperands = [...state.calcOperands];
                // console.log(calcOperands);
                calcOperands.splice(calcOperands.length - 1, 1);
                return {...state, calculations, calcOperands};
            } else {
                const calcOperators = [...state.calcOperators];
                calcOperators.splice(calcOperators.length - 1, 1);
                return {...state, calculations, calcOperators};
            }

        default: return state;
    }
};

export default reducer;