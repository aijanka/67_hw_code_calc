import React, { Component } from 'react';
import PasswordChecker from "./containers/PasswordChecker/PasswordChecker";

class App extends Component {
  render() {
    return <PasswordChecker/>
  }
}

export default App;
